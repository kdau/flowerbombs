# Release notes

[Flower Bombs](../)

## 2.0.2

*Released 2021 July 10.*

* Fix flower seeds disappearing from Flower Bombs for some players

## 2.0.1

*Released 2021 July 3.*

* Fix errors in game menus under some circumstances

## 2.0.0

*Released 2021 July 1.*

* Require, and fix compatibility with, Stardew 1.5 or higher
* Switch from PyTK to PlatoTK
* Allow Flower Bombs on the beach and other sandy areas
* Say "germinate" instead of "detonate" or "explode" throughout
* Optionally wait for next rainy or snowy day (or sprinkler) to germinate
* Germinate immediately when watered ten times
* Fix colored Wildflowers not being shown during events

## 1.0.1

*Released 2020 June 5.*

* Fix recipe unlocking instantly even when not configured to do so
* Fix forage quality and experience being applied to bomb flowers
* Bump PyTK dependency for fix to tinted rendering of Flower Bomb
* Fix glitchy rendering when a Flower Bomb is removed with a tool
* Add compatibility with Save Anywhere mod

## 1.0.0

*Released 2020 June 3.*

* Initial version
