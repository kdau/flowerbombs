[size=6][b][img]https://www.kdau.com/FlowerBombs/icon.png[/img] Flower Bombs[/b][/size]

[i]a [url=http://stardewvalley.net/]Stardew Valley[/url] mod by [url=https://www.kdau.com]kdau[/url][/i]

Make blooms, not war. Craft a Flower Bomb, fill it with wild or flower seeds, and toss it on any dirt or grass in the Valley. The next morning you’ll have an explosion of seasonal color.

[img]https://www.kdau.com/headers/compatibility.png[/img]

[b]Game:[/b] Stardew Valley 1.5+

[b]Platform:[/b] Linux, macOS or Windows

[b]Multiplayer:[/b] works; every player must install

[b]Other mods:[/b] no known conflicts; should work with vanilla and custom flowers

[img]https://www.kdau.com/headers/installation.png[/img]

[list=1]
[*]Install [url=https://smapi.io/]SMAPI[/url]
[*]Install [url=https://www.nexusmods.com/stardewvalley/mods/6589]PlatoTK[/url] (This is different from PyTK!)
[*]Install [url=https://www.nexusmods.com/stardewvalley/mods/5098]Generic Mod Config Menu[/url] (optional, for easier configuration)
[*]Download this mod from the link in the header above
[*]Unzip the mod and place the [code]FlowerBombs[/code] folder inside your [code]Mods[/code] folder
[*]Run the game using SMAPI
[/list]

[img]https://www.kdau.com/headers/use.png[/img]

Once you have two or more hearts of friendship with [b]Leah[/b], she will send you the Flower Bomb crafting recipe in the mail. (If you are married to her, she will teach you the recipe in an event in the farmhouse instead.) You can craft a Flower Bomb with [b]4 Clay and 1 Mudstone[/b].

For your convenience, [b]Clint[/b] will now sell a limited supply of Mudstone each day. If you have befriended [b]Kent[/b], he will now send you Flower Bombs in the mail instead of regular Bombs.

You can load any [b]wild or flower seeds[/b], including custom flower seeds, into a Flower Bomb the same way you would load Bait onto a fishing rod. You can unload seeds that way too. Empty Flower Bombs can stack in your inventory, but loaded Flower Bombs cannot.

You can place a Flower Bomb directly in the world on [b]any dirt or grass[/b]. To pick it up again, strike it with a tool. You can also load empty Flower Bombs into a Slingshot and fire them. If they land on or near dirt or grass, they will stick in that area.

By default, all Flower Bombs placed in the world will [b]germinate overnight[/b]. If so configured, they will not germinate until the next rainy or snowy day (or if in the range of a sprinkler). Any Flower Bomb can also be germinated immediately by watering it ten times.

When a Flower Bomb germinates that held in-season flower seeds, the growth will consist mainly of that flower, with some seasonal wildflowers and weeds at the edges. If it held in-season wild seeds, the growth will consist mainly of seasonal wildflowers with weeds at the edges. Otherwise, the growth will consist entirely of weeds. (In winter, weeds are replaced with crystals.)

[img]https://www.kdau.com/headers/configuration.png[/img]

If you have installed Generic Mod Config Menu, you can access this mod's configuration by clicking the cogwheel button at the lower left corner of the Stardew Valley title screen and then choosing "Flower Bombs".

Otherwise, you can edit this mod's [code]config.json[/code] file. It will be created in the mod's main folder ([code]Mods/FlowerBombs[/code]) the first time you run the game with the mod installed. These options are available:

[list]
[*][code]WaterToGerminate[/code]: Set this to [code]true[/code] to wait for the next rainy or snowy day (or sprinkler spray) to germinate Flower Bombs.
[*][code]ClintMudstone[/code]: Set this to [code]false[/code] to prevent Clint from offering Mudstone in his shop inventory.
[*][code]KentGifts[/code]: Set this to [code]false[/code] to allow Kent to keep sending regular Bombs in the mail.
[*][code]LeahRecipe[/code]: Set this to [code]false[/code] to unlock the Flower Bomb crafting recipe instantly instead of involving Leah.
[/list]

[img]https://www.kdau.com/headers/translation.png[/img]

This mod can be translated into any language supported by Stardew Valley. No translations are currently available.

Your contribution would be welcome. Please see the [url=https://stardewvalleywiki.com/Modding:Translations]instructions on the wiki[/url]. You can send me your work in [url=https://gitlab.com/kdau/flowerbombs/-/issues]a GitLab issue[/url] or the Posts tab above.

[img]https://www.kdau.com/headers/acknowledgments.png[/img]

[list]
[*]Like all mods, this one is indebted to ConcernedApe, Pathoschild and the various framework modders.
[*]The Flower Bomb sprite was created by Chloe Borowski.
[*]This mod was created for the Spring 2020 event on the [url=https://discordapp.com/invite/StardewValley]Stardew Valley Discord[/url].
[*]The #making-mods channel on the Discord offered valuable guidance and feedback.
[/list]

[img]https://www.kdau.com/headers/see-also.png[/img]

[list]
[*][url=https://gitlab.com/kdau/flowerbombs/-/blob/main/doc/RELEASE-NOTES.md]Release notes[/url]
[*][url=https://gitlab.com/kdau/flowerbombs]Source code[/url]
[*][url=https://gitlab.com/kdau/flowerbombs/-/issues]Report bugs[/url]
[*][url=https://www.kdau.com/stardew]My other Stardew stuff[/url]
[*]Mirrors: [b]Nexus[/b], [url=https://www.moddrop.com/stardew-valley/mods/794657]ModDrop[/url], [url=https://forums.stardewvalley.net/resources/flower-bombs.58/]forums[/url]
[/list]
Other mods to consider:

[list]
[*][url=https://www.nexusmods.com/stardewvalley/mods/2028]Mizu's Flowers[/url] offers more flower seeds you can use with the Flower Bomb.
[*]For florally-themed fun, check out the other mods from the Spring 2020 event.
[/list]
